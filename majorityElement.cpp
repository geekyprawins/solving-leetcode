//Given an array nums of size n, return the majority element.

// The majority element is the element that appears more than ⌊n / 2⌋ times. You may assume that the majority element always exists in the array.
#include <iostream>
#include <bits/stdc++.h>
using namespace std;
class Solution
{
public:
    int majorityElement(vector<int> &nums)
    {
        int n = nums.size();
        int count = 1, major = nums[0];

        for (int i = 1; i < n; i++)
        {
            if (count == 0)
            {
                count++;
                major = nums[i];
            }
            else if (major == nums[i])
            {
                count++;
            }
            else
                count--;
        }
        return major;
    }
};

int main()
{

    vector<int> arr = {2, 2, 1, 1, 1, 2, 2};
    Solution s;
    cout << "Majority element in array is : " << s.majorityElement(arr) << endl;

    return 0;
}
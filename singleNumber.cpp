// Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

// You must implement a solution with a linear runtime complexity and use only constant extra space.
#include <iostream>
#include <bits/stdc++.h>
using namespace std;

class Solution
{
public:
    int singleNumber(vector<int> &nums)
    {
        int answer = 0;
        for (auto i : nums)
        {
            answer ^= i;
        }
        return answer;
    }
};

int main()
{

    vector<int> arr = {1, 1, 5, 7, 8, 9, 9, 8, 5};
    Solution s;
    cout << " Single number in array is : " << s.singleNumber(arr) << endl;

    return 0;
}